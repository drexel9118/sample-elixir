defmodule TestProject.DigiWorld do
  @moduledoc """
  The DigiWorld context.
  """

  import Ecto.Query, warn: false
  alias TestProject.Repo

  alias TestProject.DigiWorld.DigiUsers

  @doc """
  Returns the list of digiusers.

  ## Examples

      iex> list_digiusers()
      [%DigiUsers{}, ...]

  """
  def list_digiusers do
    Repo.all(DigiUsers)
  end

  @doc """
  Gets a single digi_users.

  Raises `Ecto.NoResultsError` if the Digi users does not exist.

  ## Examples

      iex> get_digi_users!(123)
      %DigiUsers{}

      iex> get_digi_users!(456)
      ** (Ecto.NoResultsError)

  """
  def get_digi_users!(id), do: Repo.get!(DigiUsers, id)

  @doc """
  Creates a digi_users.

  ## Examples

      iex> create_digi_users(%{field: value})
      {:ok, %DigiUsers{}}

      iex> create_digi_users(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_digi_users(attrs \\ %{}) do
    %DigiUsers{}
    |> DigiUsers.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a digi_users.

  ## Examples

      iex> update_digi_users(digi_users, %{field: new_value})
      {:ok, %DigiUsers{}}

      iex> update_digi_users(digi_users, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_digi_users(%DigiUsers{} = digi_users, attrs) do
    digi_users
    |> DigiUsers.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a digi_users.

  ## Examples

      iex> delete_digi_users(digi_users)
      {:ok, %DigiUsers{}}

      iex> delete_digi_users(digi_users)
      {:error, %Ecto.Changeset{}}

  """
  def delete_digi_users(%DigiUsers{} = digi_users) do
    Repo.delete(digi_users)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking digi_users changes.

  ## Examples

      iex> change_digi_users(digi_users)
      %Ecto.Changeset{data: %DigiUsers{}}

  """
  def change_digi_users(%DigiUsers{} = digi_users, attrs \\ %{}) do
    DigiUsers.changeset(digi_users, attrs)
  end

  alias TestProject.DigiWorld.Digimon

  @doc """
  Returns the list of digimons.

  ## Examples

      iex> list_digimons()
      [%Digimon{}, ...]

  """
  def list_digimons do
    Repo.all(Digimon)
  end

  @doc """
  Gets a single digimon.

  Raises `Ecto.NoResultsError` if the Digimon does not exist.

  ## Examples

      iex> get_digimon!(123)
      %Digimon{}

      iex> get_digimon!(456)
      ** (Ecto.NoResultsError)

  """
  def get_digimon!(id), do: Repo.get!(Digimon, id)

  @doc """
  Creates a digimon.

  ## Examples

      iex> create_digimon(%{field: value})
      {:ok, %Digimon{}}

      iex> create_digimon(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_digimon(attrs \\ %{}) do
    %Digimon{}
    |> Digimon.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a digimon.

  ## Examples

      iex> update_digimon(digimon, %{field: new_value})
      {:ok, %Digimon{}}

      iex> update_digimon(digimon, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_digimon(%Digimon{} = digimon, attrs) do
    digimon
    |> Digimon.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a digimon.

  ## Examples

      iex> delete_digimon(digimon)
      {:ok, %Digimon{}}

      iex> delete_digimon(digimon)
      {:error, %Ecto.Changeset{}}

  """
  def delete_digimon(%Digimon{} = digimon) do
    Repo.delete(digimon)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking digimon changes.

  ## Examples

      iex> change_digimon(digimon)
      %Ecto.Changeset{data: %Digimon{}}

  """
  def change_digimon(%Digimon{} = digimon, attrs \\ %{}) do
    Digimon.changeset(digimon, attrs)
  end
end
