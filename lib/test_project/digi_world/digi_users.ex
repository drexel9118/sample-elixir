defmodule TestProject.DigiWorld.DigiUsers do
  use Ecto.Schema
  import Ecto.Changeset

  schema "digiusers" do
    field :age, :integer
    field :name, :string

    timestamps()
  end

  @doc false
  def changeset(digi_users, attrs) do
    digi_users
    |> cast(attrs, [:name, :age])
    |> validate_required([:name, :age])
  end
end
