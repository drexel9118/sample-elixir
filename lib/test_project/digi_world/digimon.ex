defmodule TestProject.DigiWorld.Digimon do
  use Ecto.Schema
  import Ecto.Changeset
  alias TestProject.DigiWorld.Digimon

  schema "digimons" do
    field :name, :string
    field :discription, :string
    field :level, :integer

    belongs_to :digiusers, TestProject.DigiWorld.DigiUsers

    timestamps()
  end

  @doc false
  def changeset(%Digimon{} = digimon, attrs) do
    digimon
    |> cast(attrs, [:name, :discription, :level, :digiusers_id])
    |> validate_required([:name, :discription, :level])
  end
end
