defmodule TestProjectWeb.DigimonController do
  use TestProjectWeb, :controller

  alias TestProject.DigiWorld
  alias TestProject.DigiWorld.Digimon

  action_fallback TestProjectWeb.FallbackController

  def index(conn, _params) do
    digimons = DigiWorld.list_digimons()
    render(conn, "index.json-api", data: digimons)
  end

  def create(conn, %{"digimon" => digimon_params}) do
    with {:ok, %Digimon{} = digimon} <- DigiWorld.create_digimon(digimon_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.digimon_path(conn, :show, digimon))
      |> render("show.json-api", data: digimon)
    end
  end

  def show(conn, %{"id" => id}) do
    digimon = DigiWorld.get_digimon!(id)
    render(conn, "show.json-api", data: digimon)
  end

  def update(conn, %{"id" => id, "digimon" => digimon_params}) do
    digimon = DigiWorld.get_digimon!(id)

    with {:ok, %Digimon{} = digimon} <- DigiWorld.update_digimon(digimon, digimon_params) do
      render(conn, "show.json-api", data: digimon)
    end
  end

  def delete(conn, %{"id" => id}) do
    digimon = DigiWorld.get_digimon!(id)

    with {:ok, %Digimon{}} <- DigiWorld.delete_digimon(digimon) do
      send_resp(conn, :no_content, "")
    end
  end
end
