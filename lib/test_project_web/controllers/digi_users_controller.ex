defmodule TestProjectWeb.DigiUsersController do
  use TestProjectWeb, :controller

  alias TestProject.DigiWorld
  alias TestProject.DigiWorld.DigiUsers

  action_fallback TestProjectWeb.FallbackController

  def index(conn, _params) do
    digiusers = DigiWorld.list_digiusers()
    render(conn, "index.json-api", data: digiusers)
  end

  def create(conn, %{"digi_users" => digi_users_params}) do
    with {:ok, %DigiUsers{} = digi_users} <- DigiWorld.create_digi_users(digi_users_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", Routes.digi_users_path(conn, :show, digi_users))
      |> render("show.json-api", data: digi_users)
    end
  end

  def show(conn, %{"id" => id}) do
    digi_users = DigiWorld.get_digi_users!(id)
    render(conn, "show.json-api", data: digi_users)
  end

  def update(conn, %{"id" => id, "digi_users" => digi_users_params}) do
    digi_users = DigiWorld.get_digi_users!(id)

    with {:ok, %DigiUsers{} = digi_users} <- DigiWorld.update_digi_users(digi_users, digi_users_params) do
      render(conn, "show.json-api", data: digi_users)
    end
  end

  def delete(conn, %{"id" => id}) do
    digi_users = DigiWorld.get_digi_users!(id)

    with {:ok, %DigiUsers{}} <- DigiWorld.delete_digi_users(digi_users) do
      send_resp(conn, :no_content, "")
    end
  end
end
