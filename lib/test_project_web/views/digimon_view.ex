defmodule TestProjectWeb.DigimonView do
  use TestProjectWeb, :view
  alias TestProjectWeb.DigimonView
  use JaSerializer.PhoenixView # Or use in web/web.ex

  attributes [:name, :discription, :level]

end
