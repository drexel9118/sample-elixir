defmodule TestProjectWeb.DigiUsersView do
  use TestProjectWeb, :view
  use JaSerializer.PhoenixView # Or use in web/web.ex

  attributes [:name, :age]
end
