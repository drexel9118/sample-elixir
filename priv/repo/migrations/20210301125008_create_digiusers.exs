defmodule TestProject.Repo.Migrations.CreateDigiusers do
  use Ecto.Migration

  def change do
    create table(:digiusers) do
      add :name, :string
      add :age, :integer

      timestamps()
    end

  end
end
