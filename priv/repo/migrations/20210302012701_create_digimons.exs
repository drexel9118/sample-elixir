defmodule TestProject.Repo.Migrations.CreateDigimons do
  use Ecto.Migration

  def change do
    create table(:digimons) do
      add :digiusers_id, references(:digiusers)
      add :name, :string
      add :discription, :string
      add :level, :integer
      timestamps()
    end

  end
end
