defmodule TestProjectWeb.DigimonControllerTest do
  use TestProjectWeb.ConnCase

  alias TestProject.DigiWorld
  alias TestProject.DigiWorld.Digimon

  @create_attrs %{
    name: "pokemon", discription: "a pokemon", level: 9,
  }
  @update_attrs %{
     discription: "a pokemon update", level: 1
  }
  @invalid_attrs %{
    name: nil, description: nil, level: nil
  }
  @create_user_attrs %{
    age: 42,
    name: "some name"
  }
  def fixture(:digimon) do
    {:ok, digimon} = DigiWorld.create_digimon(@create_attrs)
    digimon
  end

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/vnd.api+json")
      |> put_req_header("content-type", "application/vnd.api+json")

    {:ok, conn: conn}
  end

  describe "index" do
    test "lists all digimons", %{conn: conn} do
      conn = get(conn, Routes.digimon_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end
  @test :wip
  describe "create digimon" do
    test "renders digimon when data is valid", %{conn: conn} do
      conn = post(conn, Routes.digimon_path(conn, :create), digimon: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.digimon_path(conn, :show, id))

      assert %{
        "id" => id,
         "attributes" => %{      
          "name" => "pokemon", 
          "discription" => "a pokemon", 
          "level" => 9,
                 },
         "type" => "digimon"
            } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.digimon_path(conn, :create), digimon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update digimon" do
    setup [:create_digimon]

    test "renders digimon when data is valid", %{conn: conn, digimon: %Digimon{id: id} = digimon} do

      conn = put(conn, Routes.digimon_path(conn, :update, digimon), digimon: @update_attrs)
      assert %{"id" => id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.digimon_path(conn, :show, id))

      assert %{
        "id" => id,
         "attributes" => %{      
          "name" => "pokemon", 
          "discription" => "a pokemon update", 
          "level" => 1,
                 },
         "type" => "digimon"
            } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, digimon: digimon} do
      conn = put(conn, Routes.digimon_path(conn, :update, digimon), digimon: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete digimon" do
    setup [:create_digimon]

    test "deletes chosen digimon", %{conn: conn, digimon: digimon} do
      conn = delete(conn, Routes.digimon_path(conn, :delete, digimon))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.digimon_path(conn, :show, digimon))
      end
    end
  end

  defp create_digimon(_) do
    digimon = fixture(:digimon)
    %{digimon: digimon}
  end
end
