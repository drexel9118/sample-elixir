defmodule TestProjectWeb.DigiUsersControllerTest do
  use TestProjectWeb.ConnCase

  alias TestProject.DigiWorld
  alias TestProject.DigiWorld.DigiUsers

  @create_attrs %{
    age: 42,
    name: "some name"
  }
  @update_attrs %{
    age: 43,
    name: "some updated name"
  }
  @invalid_attrs %{age: nil, name: nil}

  def fixture(:digi_users) do
    {:ok, digi_users} = DigiWorld.create_digi_users(@create_attrs)
    digi_users
  end

  setup %{conn: conn} do
    conn =
      conn
      |> put_req_header("accept", "application/vnd.api+json")
      |> put_req_header("content-type", "application/vnd.api+json")

    {:ok, conn: conn}
  end

  describe "index" do
    test "lists all digiusers", %{conn: conn} do
      conn = get(conn, Routes.digi_users_path(conn, :index))
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create digi_users" do
    test "renders digi_users when data is valid", %{conn: conn} do
      conn = post(conn, Routes.digi_users_path(conn, :create), digi_users: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, Routes.digi_users_path(conn, :show, id))

      assert %{
        "id" => id,
         "attributes" => %{      
            "age" => 42,
            "name" => "some name"
                 },
         "type" => "digi-users"
            } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.digi_users_path(conn, :create), digi_users: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update digi_users" do
    setup [:create_digi_users]

    test "renders digi_users when data is valid", %{conn: conn, digi_users: %DigiUsers{id: id} = digi_users} do
      conn = put(conn, Routes.digi_users_path(conn, :update, digi_users), digi_users: @update_attrs)
      assert %{"id" => id} = json_response(conn, 200)["data"]

      conn = get(conn, Routes.digi_users_path(conn, :show, id))

      assert %{
               "id" => id,
               "attributes" => %{      
                 "age" => 43,
                  "name" => "some updated name"
                  },
                "type" => "digi-users"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, digi_users: digi_users} do
      conn = put(conn, Routes.digi_users_path(conn, :update, digi_users), digi_users: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete digi_users" do
    setup [:create_digi_users]

    test "deletes chosen digi_users", %{conn: conn, digi_users: digi_users} do
      conn = delete(conn, Routes.digi_users_path(conn, :delete, digi_users))
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, Routes.digi_users_path(conn, :show, digi_users))
      end
    end
  end

  defp create_digi_users(_) do
    digi_users = fixture(:digi_users)
    %{digi_users: digi_users}
  end
end
