defmodule TestProject.DigiWorldTest do
  use TestProject.DataCase

  alias TestProject.DigiWorld

  describe "digiusers" do
    alias TestProject.DigiWorld.DigiUsers

    @valid_attrs %{age: 42, name: "some name"}
    @update_attrs %{age: 43, name: "some updated name"}
    @invalid_attrs %{age: nil, name: nil}

    def digi_users_fixture(attrs \\ %{}) do
      {:ok, digi_users} =
        attrs
        |> Enum.into(@valid_attrs)
        |> DigiWorld.create_digi_users()

      digi_users
    end

    test "list_digiusers/0 returns all digiusers" do
      digi_users = digi_users_fixture()
      assert DigiWorld.list_digiusers() == [digi_users]
    end

    test "get_digi_users!/1 returns the digi_users with given id" do
      digi_users = digi_users_fixture()
      assert DigiWorld.get_digi_users!(digi_users.id) == digi_users
    end

    test "create_digi_users/1 with valid data creates a digi_users" do
      assert {:ok, %DigiUsers{} = digi_users} = DigiWorld.create_digi_users(@valid_attrs)
      assert digi_users.age == 42
      assert digi_users.name == "some name"
    end

    test "create_digi_users/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = DigiWorld.create_digi_users(@invalid_attrs)
    end

    test "update_digi_users/2 with valid data updates the digi_users" do
      digi_users = digi_users_fixture()
      assert {:ok, %DigiUsers{} = digi_users} = DigiWorld.update_digi_users(digi_users, @update_attrs)
      assert digi_users.age == 43
      assert digi_users.name == "some updated name"
    end

    test "update_digi_users/2 with invalid data returns error changeset" do
      digi_users = digi_users_fixture()
      assert {:error, %Ecto.Changeset{}} = DigiWorld.update_digi_users(digi_users, @invalid_attrs)
      assert digi_users == DigiWorld.get_digi_users!(digi_users.id)
    end

    test "delete_digi_users/1 deletes the digi_users" do
      digi_users = digi_users_fixture()
      assert {:ok, %DigiUsers{}} = DigiWorld.delete_digi_users(digi_users)
      assert_raise Ecto.NoResultsError, fn -> DigiWorld.get_digi_users!(digi_users.id) end
    end

    test "change_digi_users/1 returns a digi_users changeset" do
      digi_users = digi_users_fixture()
      assert %Ecto.Changeset{} = DigiWorld.change_digi_users(digi_users)
    end
  end

  describe "digimons" do
    alias TestProject.DigiWorld.Digimon

    @valid_attrs %{name: "pokemon", discription: "a pokemon", level: 9,}
    @update_attrs %{name: "pokemon", discription: "a pokemon update", level: 1}
    @invalid_attrs %{name: nil, description: nil, level: nil}

    def digimon_fixture(attrs \\ %{}) do
      {:ok, digimon} =
        attrs
        |> Enum.into(@valid_attrs)
        |> DigiWorld.create_digimon()

      digimon
    end

    test "list_digimons/0 returns all digimons" do
      digimon = digimon_fixture()
      assert DigiWorld.list_digimons() == [digimon]
    end

    test "get_digimon!/1 returns the digimon with given id" do
      digimon = digimon_fixture()
      assert DigiWorld.get_digimon!(digimon.id) == digimon
    end

    test "create_digimon/1 with valid data creates a digimon" do
      assert {:ok, %Digimon{} = digimon} = DigiWorld.create_digimon(@valid_attrs)
    end

    test "create_digimon/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = DigiWorld.create_digimon(@invalid_attrs)
    end

    test "update_digimon/2 with valid data updates the digimon" do
      digimon = digimon_fixture()
      assert {:ok, %Digimon{} = digimon} = DigiWorld.update_digimon(digimon, @update_attrs)
    end

    test "update_digimon/2 with invalid data returns error changeset" do
      digimon = digimon_fixture()
      assert {:error, %Ecto.Changeset{}} = DigiWorld.update_digimon(digimon, @invalid_attrs)
      assert digimon == DigiWorld.get_digimon!(digimon.id)
    end

    test "delete_digimon/1 deletes the digimon" do
      digimon = digimon_fixture()
      assert {:ok, %Digimon{}} = DigiWorld.delete_digimon(digimon)
      assert_raise Ecto.NoResultsError, fn -> DigiWorld.get_digimon!(digimon.id) end
    end
    @tag :wip
    test "change_digimon/1 returns a digimon changeset" do
      digimon = digimon_fixture()
      assert %Ecto.Changeset{} = DigiWorld.change_digimon(digimon)
    end
  end
end
